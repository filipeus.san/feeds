import 'reflect-metadata';
import { Container } from 'typedi';
import initializeDB from '../database/index';
import csvParser from 'csv-parser';
import fs from 'fs';
import TokenService from '../services/tokenService';


async function main() {
    await initializeDB();
    const tokenService = Container.get(TokenService);
    const tokens: {value: string, emotion: number}[] = [];
  

    fs.createReadStream('src/data/sublex_1_0.csv')
    .pipe(csvParser({ separator: '\t' }))
    .on('data', (data) => {
      
        let value = data.slovo;
        let emotion = 0;

        if (data.emoce === "POS") {
            emotion = 1; 
        }

        tokens.push({value: value, emotion: emotion});
    })
    .on('end', async () => {
        console.log("--- Tokens add:");

        for (const c in tokens) {
            const token  = tokens[c];
            await tokenService.add(token.value, token.emotion);
            console.log(token);
        }
        process.exit()
    });
} 


main();