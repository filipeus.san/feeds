import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,

} from 'typeorm';

@Entity()
export class Provider extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column("text")
  description: string;

  @Column()
  rssUrl: string;

  @Column("text")
  htmlQuery: string;
}
