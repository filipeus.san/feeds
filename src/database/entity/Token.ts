import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,

} from 'typeorm';

@Entity()
export class Token extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: string;

  @Column()
  emotion: number;
}
