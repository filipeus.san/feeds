import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  ManyToOne
} from 'typeorm';
import { Provider } from './Provider';

@Entity()
export class Article extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column("text")
  content: string;

  @Column()
  url: string;

  @Column({ default: 0})
  emotionScore: Number;  

  @ManyToOne(() => Provider)
  provider: Provider;
}