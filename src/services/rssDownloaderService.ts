import Parser from 'rss-parser';
import { Service } from 'typedi';
import { Article } from '../database/entity/Article';
import ProviderService from './providerService';

@Service()
class RssDownloaderService {
    private parser: Parser;

    constructor(
        private readonly providerService: ProviderService,
    ) {
        this.parser = new Parser();
    }

    public async download (): Promise<void>  {
        console.log("start");
        const providers = await this.providerService.getList();
        
        providers.forEach(async provider => {
            const feed = await this.parser.parseURL(provider.rssUrl);
            for (const c in feed.items) {
                const item = feed.items[c];
                const count = await Article.count({where: {url: item.link}});
                
                if (count === 0) {
                    await Article.create({title: item.title, content: "", url: item.link, provider: provider}).save();
                }
            }
        });
    }
}    

export default RssDownloaderService;