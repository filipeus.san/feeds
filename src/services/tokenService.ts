import { Service } from 'typedi';
import { Token } from '../database/entity/Token';


@Service()
export class TokenService {


  getList = async (): Promise<Token[]> => {
    return await Token.find({});
  }

  add = async (value: string, emotion: number): Promise<boolean> => {
   
    const entity = {
      value: value,
      emotion: emotion
    };

    const keyWord = await Token.findOne({where: {value: value, emotion: emotion}});
    
    if (keyWord) {
      return false;
    }

    await Token.create(entity).save();

    return true;
  }
}
 
export default TokenService;
 