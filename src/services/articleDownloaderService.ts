import htmlParse from 'node-html-parser';
import { Service } from 'typedi';
import { Article } from '../database/entity/Article';
import fetch, { Headers } from 'node-fetch';
import utf8 from 'utf8';
import ArticleAnalyseService from './articleAnalyseService';

@Service()
class ArticleDownloaderService {

    public constructor(
        private readonly articleAnalyseService: ArticleAnalyseService,
    ) {
        
    }

    public async download (): Promise<void>  {
       const articles = await Article.createQueryBuilder('article')
       .where('article.content = ""')
       .getMany();

       for (const c in articles) {;
            const article = articles[c];

            const res = await fetch(article.url);

            const html = await res.text();
            const parser = htmlParse(html);

            console.log(article.provider);
           /** 
           const elementContent = parser.querySelector(htmlQuery.content);
            if (elementContent) {
                article.content  = elementContent.innerText.replace(/(\r\n|\n|\r)/gm,"");
                const emotionScore = await this.articleAnalyseService.analyse(article.content);
                article.emotionScore = emotionScore;
            }

            Article.save(article);
            */
       }
    }
}    

export default ArticleDownloaderService;