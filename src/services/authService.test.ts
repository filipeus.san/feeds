import { User } from '../database/entity/User';
import { AuthService } from './authService';
import bcrypt from "bcrypt";
import { AuthToken } from '../schema/user';


test("login - user not exists", async ()=>{
    const myMock = jest.fn();
 
    myMock.mockReturnValue(null);
    User.findOne = myMock;
    
    const authService = new AuthService();
    try {
        await authService.login({email: "test@test.cz", password: "Heslo_12346"})
    } catch (error) {
        expect(error).toBeInstanceOf(Error);
    }
})


test("login", async ()=>{
    const myMock = jest.fn();

     const salt = bcrypt.genSaltSync(10);

    myMock.mockReturnValue(
        {
            id: 1,
            name: "Filip",
            lastname: "Tomek",
            email: "test@test.cz",
            password: "$2b$10$FDZcAHlPyeF5QlbF6HCo1.FXDC/a1dLNpEcxx22LCuujoHojBIfb6",
        }
    );

    User.findOne = myMock;
    
    const authService = new AuthService();
    const login = await authService.login({email: "test@test.cz", password: "Heslo_12346"})
    expect(login).toBeInstanceOf(AuthToken);
})