import fetch from 'node-fetch';
import { Service } from 'typedi';
import csvParser from 'csv-parser';
import { Readable } from 'stream';
import { Token } from '../database/entity/Token';
import e from 'express';


@Service()
class ArticleAnalyseService {

    public async analyse (content: string): Promise<Number>  {
       const res = await fetch('http://lindat.mff.cuni.cz/services/morphodita/api/tag?data=' + encodeURI(content.substring(0, 1000)) + '.&output=vertical');
       const json = await res.json();
       const csv = 's1\ttoken\ts2\n' + json.result; 
       const readable = Readable.from([csv])

       let emotionScore = 0;
       const tokens: String[] = [];


       const myPromise: Promise<Number> = new Promise((resolve, reject) => {
        readable
        .pipe(csvParser({ separator: '\t' }))
        .on('data', async (data) => {
            const value = data.token;
            tokens.push(value);
        })
        .on('end', async () => {
                
            for (const c in tokens) {
                const value = tokens[c];
                const token = await Token.findOne({where:{value: value}})
                if (token) {
                    if (token.emotion === 1) {
                        emotionScore++;
                    } else {
                        emotionScore--;
                    }
                }
            }
            resolve(emotionScore);
        }); 
       });

       return await myPromise;
    }
}    

export default ArticleAnalyseService;