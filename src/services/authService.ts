import { Service } from 'typedi';
import { User } from '../database/entity/User';
import { LoginUserInput, RegisterUserInput, AuthToken } from '../schema/user';
import bcrypt from "bcrypt";
import { randomBytes } from 'crypto';

@Service()
export class AuthService {

  authTokens: Array<AuthToken> = [];

  login = async (LoginUserInput: LoginUserInput): Promise<AuthToken> => {
    const user = await User.findOne({ where: { email: LoginUserInput.email } });

    if (!user) {
      throw new Error(`Špatné uživatelské jméno nebo heslo`);
    }

    if (!bcrypt.compareSync(LoginUserInput.password, user.password)) {
      throw new Error(`Špatné uživatelské jméno nebo heslo`);
    }

    return this.genAuthToken(user.id);
  }

  getUser = async (token: string): Promise<User>  => {
    
    for (const c in this.authTokens) {
      const item = this.authTokens[c];
      if (item.token === token) {
        const user = await User.findOne({ where: { id: item.id } });
        
        if (!user) {
          throw new Error(`Uživatel neexistuje`);
        }
        
        return user;
      } 
    }

    throw new Error(`Uživatel není přihlášen`);
  }

  isLogged = (token: string): number | undefined => {

    for (const c in this.authTokens) {
      const item = this.authTokens[c];
      if (item.token === token) {
        return item.id;
      }  
    }  

    throw new Error(`Uživatel není přihlášen`);
  }

  register = async (RegisterUserInput: RegisterUserInput): Promise<boolean> => {
    const user = await User.findOne({ where: { email: RegisterUserInput.email } });

    if (user) {
      throw new Error(`Uživatel s tímto emailem existuje`);
    }

    if (RegisterUserInput.password !== RegisterUserInput.password2) {
      throw new Error(`Zadané hesla se neshodují`);
    } 

    const salt = bcrypt.genSaltSync(10);
    RegisterUserInput.password = bcrypt.hashSync(RegisterUserInput.password, salt);

    await User.create(RegisterUserInput).save();

    return true;
  }

  genAuthToken = (id: number): AuthToken => {
    const token = randomBytes(28).toString('hex');

    const authToken = new AuthToken();
    authToken.id = id;    
    authToken.token = token;

    this.authTokens.push(authToken);

    return authToken;
  }
}
 
export default AuthService;
 