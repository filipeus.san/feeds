import { Service } from 'typedi';
import { Provider } from '../database/entity/Provider';


@Service()
export class ProviderService {

  getList = async (): Promise<Provider[]> => {
    return await Provider.find({});
  }
}
 
export default ProviderService;
 