import { CronJob } from 'cron';
import { Service } from 'typedi';
import ArticleDownloaderService from '../services/articleDownloaderService';
import RssDownloaderService from '../services/rssDownloaderService';

@Service()
class DownloaderCron {

    cronJob: CronJob;
  
    constructor(
        private readonly rssDownloader: RssDownloaderService,
        private readonly articleDownloader: ArticleDownloaderService
      ) {
      this.cronJob = new CronJob('*/10 * * * * *', async () => {
        try {
          await this.proccess();
        } catch (e) {
          console.error(e);
        }
      });
    }

     public async proccess(): Promise<void> {
        await this.rssDownloader.download();
        await this.articleDownloader.download();
     }

     public async run(): Promise<void> {

      if (!this.cronJob.running) {
        console.log("runnig RssDonwloadCron");
        this.cronJob.start();
      }

    }
}

export default DownloaderCron;