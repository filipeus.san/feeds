import 'reflect-metadata';
import { Container } from 'typedi';
import DownloaderCron from './downloaderCron';
import initializeDB from '../database/index';

async function main() {
    await initializeDB();

    const rssDonwloadCron = Container.get(DownloaderCron);
    await rssDonwloadCron.proccess();
}  


main();