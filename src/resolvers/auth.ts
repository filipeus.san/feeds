import { Query, Resolver, Mutation, Arg } from 'type-graphql';
import { Service } from 'typedi';
import { UserSchema, RegisterUserInput, LoginUserInput, AuthToken  } from '../schema/user';
import { AuthService } from '../services/authService';

@Service()
@Resolver()
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation((returns) => AuthToken)
  async userLogin(
    @Arg('LoginUserInput') LoginUserInput: LoginUserInput,
  ): Promise<AuthToken | undefined> {
    return await this.authService.login(LoginUserInput);
  }

  @Mutation((returns) => Boolean)
  async userRegister(
    @Arg('RegisterUserInput') RegisterUserInput: RegisterUserInput,
  ): Promise<boolean> {
    return await this.authService.register(RegisterUserInput);
  }

  @Query((returns) => UserSchema)
  async getUser(@Arg('token') token: string): Promise<UserSchema> {
    return await this.authService.getUser(token);
  }
}
