import { Field, ObjectType, InputType } from 'type-graphql';
import {IsEmail, Length } from 'class-validator';

@ObjectType()
export class UserSchema {
  @Field()
  id: number;

  @Field()
  name: string;

  @Field()
  lastname: string;

  @Field()
  email: string;
}


@InputType()
export class LoginUserInput implements Partial<UserSchema> {
  @Field()
  @Length(1, 50 , {
    message: 'Musíte zadat email',
  })
  @IsEmail(undefined, { message: "Email není ve správném formátu"})
  email: string;

  @Field()
  @Length(1, 250 , {
    message: 'Musíte zadat heslo',
  })
  password: string;
}

@InputType()
export class RegisterUserInput implements Partial<UserSchema> {
  
  @Field()
  @IsEmail(undefined, { message: "Email není ve správném formátu"})
  @Length(1, 50, {
    message: 'Musíte zadat email',
  })
  email: string;
  
  @Field()
  @Length(1, 50, {
    message: 'Musíte zadat jméno',
  })
  name: string;

  @Field()
  @Length(1, 50, {
    message: 'Musíte zadat příjmení',
  })
  lastname: string;

  @Field()
  @Length(1, 250, {
    message: 'Musíte zadat heslo',
  })
  password: string;

  @Field()
  @Length(1, 250, {
    message: 'Musíte zadat heslo znovu',
  })
  password2: string;
}

@ObjectType()
export class AuthToken {
  @Field()
  id: number;

  @Field()
  token: string;
}
